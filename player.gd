class_name Player
extends CharacterBody2D

const GRAVITY = 600
const WALK_SPEED = 150
const JUMP_FORCE = 250

var double_jump = true
var walk = false
var duck = true


func _ready():
	return

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	
	duck = Input.is_action_pressed("ui_down") and is_on_floor()
	
	if duck:
		walk = false
	elif Input.is_action_pressed("ui_left"):
		get_node("Sprite2D").flip_h = true
		if !duck:
			to_walk(-1)
	elif Input.is_action_pressed("ui_right"):
		get_node("Sprite2D").flip_h = false
		if !duck:
			to_walk(1)
	else:
		walk = false
	
	if !walk:
		if velocity.x < 0.01 and velocity.x > -0.01:
			velocity.x = 0
		else:
			velocity.x = velocity.x * 0.8

	if Input.is_action_just_pressed("ui_up"):
		if is_on_floor() or is_on_wall(): 
			double_jump = true
			to_jump()
		elif double_jump:
			double_jump = false
			to_jump()
	
	set_velocity(velocity)
	set_up_direction(Vector2.UP)
	move_and_slide()
	velocity = velocity
	
	# Animation
	if velocity.y != 0:
		if velocity.y > 0:
			if is_on_wall():
				get_node("Sprite2D").flip_h = !get_node("Sprite2D").flip_h
			get_node("Sprite2D/AnimationPlayer").play("fall")
		else:
			get_node("Sprite2D/AnimationPlayer").play("jump")
	elif duck:
		get_node("Sprite2D/AnimationPlayer").play("duck")
	elif walk:
		get_node("Sprite2D/AnimationPlayer").play("walk")
	else:
		get_node("Sprite2D/AnimationPlayer").play("idle")

func to_jump():
	velocity.y = -JUMP_FORCE
	
func to_walk(m):
	velocity.x = m * WALK_SPEED 
	walk = true
